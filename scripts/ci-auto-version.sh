#!/bin/sh

git update-ref refs/heads/ci-processing HEAD
git symbolic-ref HEAD refs/heads/ci-processing
git config user.name "Radu CI"
git config user.email "radu.creosteanu@gmail.com"
git stash
git pull origin master
npm run $1
git push --tags https://creosteanu:${CI_TOKEN}@gitlab.com/screeps-united-nations/hydra.git ci-processing:${CI_COMMIT_REF_NAME}
