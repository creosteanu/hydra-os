/* eslint-disable no-console, no-sync, no-process-exit */

let modConcat = require('module-concat');
let fs = require('fs');

let outputDir = './dist/';
let outputFile = 'main.js';

try {
    fs.mkdirSync(outputDir);
} catch (e) {
    // yum yum
}

modConcat('./src/main.js', outputDir + outputFile, (err, stats) => {
    if (err) {
        console.log(`Error while processing json: ${ err.message}`);
        process.exit(1);
    } else {
        console.log(`${stats.files.length } were combined into ${ outputFile}`);
    }
});

/* eslint-enable no-console, no-sync, no-process-exit */
