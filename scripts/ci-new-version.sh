#!/bin/sh
NEWVER=`semver $npm_package_version -i $1`
git tag -d v$NEWVER 2> /dev/null
npm version $NEWVER -m "Increment version to v%s [skip ci]"
