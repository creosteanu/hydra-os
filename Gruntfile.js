let matchdep = require('matchdep');

let config = require('./.screeps.json');

// eslint-disable-next-line max-lines-per-function
function gruntfile(grunt) {
    matchdep.filterAll(['grunt-*', '!grunt-cli']).forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        screeps: {
            options: config,
            dist:    {
                src: ['dist/*.js']
            }
        },

        copy: {
            main: {
                expand:  true,
                flatten: true,
                filter:  'isFile',
                cwd:     'dist/',
                src:     '**',
                dest:    config.privateDirectory
            }
        },

        shell: {
            command: 'npm run build'
        }
    });

    grunt.registerTask('write-private', 'copy');
    grunt.registerTask('build', 'shell');
    grunt.registerTask('private', ['build', 'write-private']);
}

module.exports = gruntfile;
