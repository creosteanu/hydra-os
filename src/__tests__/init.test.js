test('updates version', () => {
    jest.mock('../../package', () => ({version: '0.0.2'}));

    let {init} = require('../init');

    let memory = {version: '0.0.1'};
    init(memory);

    expect(memory.version).toBe('0.0.2');
});

test('instantiates operationsLogic in memory', () => {
    let {init} = require('../init');

    let memory = {};
    init(memory);

    expect(memory.operations).toBeDefined();
});

test('instantiates core operationsLogic in memory', () => {
    let {init} = require('../init');

    let memory = {};
    let operationsLogic = {coreOperation: {core: true}, nonCoreOperation: {}};
    init(memory, operationsLogic);

    expect(memory.operations.coreOperation).toEqual({});
    expect(memory.operations.nonCoreOperation).toBeUndefined();
});

test('executes core operation init', () => {
    let {init} = require('../init');

    let mockFunction = jest.fn(() => ({initialized: true}));

    let memory = {};
    let operationsLogic = {coreOperation: {core: true, init: mockFunction}};
    init(memory, operationsLogic);
    init(memory, operationsLogic);

    expect(mockFunction).toHaveBeenCalled();
    expect(memory.operations.coreOperation).toEqual({initialized: true});
});
