let {execute} = require('../execute');

test('executes operation in memory', () => {
    let operationsLogic = {testOperation: jest.fn()};
    let memory = {operations: {testOperationId: {name: 'testOperation'}}};

    execute(memory, operationsLogic);

    expect(operationsLogic.testOperation).toHaveBeenCalled();
    expect(operationsLogic.testOperation).toHaveBeenCalledWith({id: 'testOperationId', memory: {name: 'testOperation'}});
});

test('operation name falls back to id if name is missing', () => {
    let operationsLogic = {testOperation: jest.fn()};
    let memory = {operations: {testOperation: {}}};

    execute(memory, operationsLogic);

    expect(operationsLogic.testOperation).toHaveBeenCalled();
    expect(operationsLogic.testOperation).toHaveBeenCalledWith({id: 'testOperation', memory: {}});
});

test('executes operations safely', () => {
    let operationsLogic = {testOperation: jest.fn(() => { throw 'error'; })};
    let memory = {operations: {testOperationId: {name: 'testOperation'}}};
    let consoleSpy = jest.spyOn(console, 'log').mockReturnValue();

    execute(memory, operationsLogic);

    expect(operationsLogic.testOperation).toHaveBeenCalled();
    expect(operationsLogic.testOperation).toHaveBeenCalledWith({id: 'testOperationId', memory: {name: 'testOperation'}});
    expect(consoleSpy).toHaveBeenCalledWith('error');
});
