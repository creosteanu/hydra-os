test('basic run', () => {
    let consoleSpy = jest.spyOn(console, 'log');
    let {loop} = require('../main');
    StructureSpawn.prototype.spawnCreep = () => ERR_BUSY;

    loop();
    loop();

    expect(consoleSpy).not.toHaveBeenCalled();
});

test('has required extensions', () => {
    require('../main');

    expect(Room.prototype.cachedFind).toBeDefined();
});

test('resets cache every tick', () => {
    let {cache} = require('cache');
    cache.data.testKey = 'testValue';
    let {loop} = require('../main');

    loop();

    expect(cache.data.testKey).toBeUndefined();
});
