let {transformObject} = require('js-utils');

test('executes callback', () => {
    let mockFunction = jest.fn();
    let mockObject = {key: 'value'};

    transformObject(mockObject, mockFunction);

    expect(mockFunction).toHaveBeenCalled();
    expect(mockFunction).toHaveBeenCalledWith({}, 'value', 'key');
});

test('exits early when returning false', () => {
    let mockFunction = jest.fn(() => false);
    let mockObject = {key: 'value', secondKey: 'value'};

    transformObject(mockObject, mockFunction);

    expect(mockFunction).toHaveBeenCalled();
});

test('iterates all properties', () => {
    let mockFunction = jest.fn();
    let mockObject = {key: 'value', secondKey: 'value'};

    transformObject(mockObject, mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(2);
});

test('does not crash with null object', () => {
    expect(() => transformObject(null, () => {})).not.toThrow();
});

test('returns new object', () => {
    let mockFunction = (acc, value, key) => { acc[value] = key; };
    let mockObject = {key: 'value', secondKey: 'value'};

    let result = transformObject(mockObject, mockFunction, {});

    expect(result).toEqual({value: 'key'});
});
