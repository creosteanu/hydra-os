let {sumArray} = require('js-utils');

test('returns sum of elements', () => {
    let mockArray = [1, 2, 0, 47, 4, 4, 4];

    let result = sumArray(mockArray, v => v);

    expect(result).toBe(62);
});
