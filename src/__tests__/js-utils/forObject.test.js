let {forObject} = require('js-utils');

test('executes callback', () => {
    let mockFunction = jest.fn();

    forObject({key: 'value'}, mockFunction);

    expect(mockFunction).toHaveBeenCalled();
    expect(mockFunction).toHaveBeenCalledWith('value', 'key');
});

test('exits early when returning false', () => {
    let mockFunction = jest.fn(() => false);

    forObject({key: 'value', secondKey: 'value'}, mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(1);
});

test('iterates all properties', () => {
    let mockFunction = jest.fn();

    forObject({key: 'value', secondKey: 'value'}, mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(2);
});

test('does not crash with null object', () => {
    expect(() => forObject(null, () => {})).not.toThrow();
});
