let {forArray} = require('js-utils');

test('executes callback', () => {
    let mockFunction = jest.fn();

    forArray(['value'], mockFunction);

    expect(mockFunction).toHaveBeenCalled();
    expect(mockFunction).toHaveBeenCalledWith('value', 0);
});

test('exits early when returning false', () => {
    let mockFunction = jest.fn(() => false);

    forArray(['value', 'secondValue'], mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(1);
});

test('iterates all properties', () => {
    let mockFunction = jest.fn();

    forArray(['value', 'secondValue'], mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(2);
});

test('does not crash with null object', () => {
    expect(() => forArray(null, () => {})).not.toThrow();
});
