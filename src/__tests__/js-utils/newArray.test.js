let {newArray} = require('js-utils');

test('creates new array of right length', () => {
    let mockArray = newArray(3, () => {});

    expect(mockArray).toHaveLength(3);
});

test('creates new array with right objects', () => {
    let mockArray = newArray(3, () => 1);

    expect(mockArray[2]).toBe(1);
});
