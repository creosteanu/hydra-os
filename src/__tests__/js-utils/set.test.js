let {set} = require('js-utils');

test('sets property', () => {
    let mockObject = {};

    set(mockObject, ['key'], 'value');

    expect(mockObject.key).toBe('value');
});

test('sets nested property', () => {
    let mockObject = {};

    set(mockObject, ['key', 'anotherKey'], 'value');

    expect(mockObject.key.anotherKey).toBe('value');
});
