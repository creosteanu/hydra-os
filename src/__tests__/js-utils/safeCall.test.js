let {safeCall} = require('js-utils');

test('executes process', () => {
    let mockFunction = jest.fn();

    safeCall(mockFunction, []);

    expect(mockFunction).toHaveBeenCalled();
});

test('returns process result', () => {
    let mockFunction = jest.fn();
    mockFunction.mockReturnValueOnce(true);

    let result = safeCall(mockFunction, []);

    expect(result).toBeDefined();
});

test('passes arguments to process', () => {
    let mockFunction = jest.fn();

    safeCall(mockFunction, ['arg1', 'arg2']);

    expect(mockFunction).toHaveBeenCalledWith('arg1', 'arg2');
});

test('logs exceptions', () => {
    function exceptionGenerator() {
        throw 'exception';
    }

    let consoleSpy = jest.spyOn(console, 'log').mockReturnValue();

    safeCall(exceptionGenerator, []);

    expect(consoleSpy).toHaveBeenCalledWith('exception');
});
