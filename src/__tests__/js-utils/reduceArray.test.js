let {reduceArray} = require('js-utils');

test('executes callback', () => {
    let mockFunction = jest.fn();
    let mockObject = ['value'];

    reduceArray(mockObject, mockFunction);

    expect(mockFunction).toHaveBeenCalled();
    expect(mockFunction).toHaveBeenCalledWith({}, 'value', 0);
});

test('iterates all properties', () => {
    let mockFunction = jest.fn();
    let mockObject = ['value', 'secondValue'];

    reduceArray(mockObject, mockFunction);

    expect(mockFunction).toHaveBeenCalledTimes(2);
});

test('does not crash with null object', () => {
    expect(() => reduceArray(null, () => {})).not.toThrow();
});

test('returns new object', () => {
    let mockFunction = (acc, value, key) => {
        acc[value] = key;

        return acc;
    };

    let mockObject = ['value', 'secondValue'];

    let result = reduceArray(mockObject, mockFunction, {});

    expect(result).toEqual({secondValue: 1, value: 0});
});
