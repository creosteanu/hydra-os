let {get} = require('js-utils');

test('retrieves property', () => {
    let mockObject = {key: 'value'};

    let value = get(mockObject, ['key']);

    expect(value).toBe('value');
});

test('retrieves nested property', () => {
    let mockObject = {key: {anotherKey: 'value'}};

    let value = get(mockObject, ['key', 'anotherKey']);

    expect(value).toBe('value');
});

test('returns default when property missing', () => {
    let mockObject = {key: 'value'};

    let value = get(mockObject, ['noKey'], 'default');

    expect(value).toBe('default');
});
