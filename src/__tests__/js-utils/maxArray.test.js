let {maxArray} = require('js-utils');

test('executes callback', () => {
    let mockFunction = jest.fn();
    let mockArray = ['value'];

    maxArray(mockArray, mockFunction);

    expect(mockFunction).toHaveBeenCalled();
    expect(mockFunction).toHaveBeenCalledWith('value');
});

test('returns max element', () => {
    let mockArray = [1, 2, 0, 47, 4, 4, 4];

    let result = maxArray(mockArray);

    expect(result).toBe(47);
});

test('returns max element based on computation', () => {
    let mockArray = [1, 2, 0, 47, 4, 4, 4];

    let result = maxArray(mockArray, value => -value);

    expect(result).toBe(0);
});
