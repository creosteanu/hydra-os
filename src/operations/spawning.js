let {transformObject, forObject, maxArray, findArray} = require('js-utils');
let {cache} = require('cache');

function checkSpawn(memory, data, creepName) {
    delete memory.shouldSpawn[creepName];

    if (!Game.creeps[creepName]) {
        console.log(`spawnFailed ${creepName} ${JSON.stringify(data)}`); // eslint-disable-line no-console
        memory.scheduledSpawn[creepName] = data;
    }
}

function getAvailableSpawn(roomName) {
    return findArray(Game.rooms[roomName].findMyStructures(STRUCTURE_SPAWN), spawn => !spawn.spawning);
}

function getCreepToSpawn(scheduledSpawn) {
    if (scheduledSpawn.length <= 1) {
        return scheduledSpawn[0];
    } else {
        return maxArray(scheduledSpawn, spawn => spawn.data.priority);
    }
}

function attemptSpawn(memory, roomSpawnSchedule, roomName) {
    let {creepName, data} = getCreepToSpawn(roomSpawnSchedule);

    let spawn = getAvailableSpawn(roomName);
    if (spawn.room.energyAvailable >= data.energy) {
        if (spawn.spawnCreep(data.body, creepName) === OK) {
            memory.shouldSpawn = memory.shouldSpawn || {};
            memory.shouldSpawn[creepName] = data;

            delete memory.scheduledSpawn[creepName];
            delete Memory.creeps;
        }
    }
}

function roomCanSpawn(room, data) {
    let cacheKey = `${room.name}-canSpawn`;
    if (cache.data[cacheKey] === undefined) {
        cache.data[cacheKey] = getAvailableSpawn(room.name);
    }

    return cache.data[cacheKey] && room.energyCapacityAvailable >= data.energy;
}

function shouldSpawn(roomSchedules, data, creepName) {
    if (data.time <= Game.time) {
        let {roomName} = data;
        if (roomCanSpawn(Game.rooms[roomName], data)) {
            roomSchedules[roomName] = roomSchedules[roomName] || [];
            roomSchedules[roomName].push({creepName, data});
        }
    }
}

function spawning({memory}) {
    memory.scheduledSpawn = memory.scheduledSpawn || {};

    forObject(memory.shouldSpawn, checkSpawn.bind(null, memory));

    let roomSchedules = transformObject(memory.scheduledSpawn, shouldSpawn);

    forObject(roomSchedules, attemptSpawn.bind(null, memory));
}

spawning.core = true;

module.exports = {spawning};
