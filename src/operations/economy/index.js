let {farmer} = require('./farmer');
let {farming} = require('./farming');
let {pioneering} = require('./pioneering');

module.exports = {farmer, farming, pioneering};
