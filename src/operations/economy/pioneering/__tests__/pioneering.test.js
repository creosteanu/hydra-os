let {objectSize} = require('js-utils');

let pioneering;
let creep;
beforeEach(() => {
    pioneering = require('../index').pioneering;

    creep = new Creep({
        pos:  new RoomPosition(1, 3, 'W15S34'),
        name: 'W15S34-pioneer-0',
        body: [{type: MOVE, hits: 100}, {type: CARRY, hits: 100}, {type: CARRY, hits: 100}, {type: WORK, hits: 100}]
    });
});

test('is core', () => {
    expect(pioneering.core).toBeTruthy();
});

test('is part of operations object', () => {
    let {operationsLogic} = require('../../../index');

    expect(operationsLogic.pioneering).toBeDefined();
});

test('schedules pioneer spawns', () => {
    new Source({pos: new RoomPosition(5, 5, 'W15S34')});
    let memory = {};

    pioneering({memory});

    expect(memory.W15S34).toEqual({'W15S34-pioneer-1': {}});

    for (let i = 0; i < 10; i++) {
        pioneering({memory});
    }

    expect(objectSize(memory.W15S34)).toBe(3);
});

test('creep starts extraction', () => {
    let memory = {W15S34: {'W15S34-pioneer-0': {}}};

    pioneering({memory});

    expect(memory.W15S34['W15S34-pioneer-0'].task).toBe('extractEnergy');
});

test('creep starts energy distribution', () => {
    new StructureExtension({id: 'Extension1', pos: new RoomPosition(10, 11, 'W15S34')});
    new StructureExtension({id: 'Extension2', pos: new RoomPosition(11, 12, 'W15S34')});
    creep.carry[RESOURCE_ENERGY] = 100;
    let memory = {W15S34: {'W15S34-pioneer-0': {}}};

    pioneering({memory});

    expect(memory.W15S34['W15S34-pioneer-0'].task).toBe('distributeEnergy');
});

test('creep starts controller upgrade', () => {
    creep.carry[RESOURCE_ENERGY] = 100;
    creep.room.energyAvailable = creep.room.energyCapacityAvailable;
    let memory = {W15S34: {'W15S34-pioneer-0': {}}};

    pioneering({memory});

    expect(memory.W15S34['W15S34-pioneer-0'].task).toBe('upgradeController');
});

test('creep starts building', () => {
    new ConstructionSite({pos: new RoomPosition(1, 2, 'W15S34')});
    creep.carry[RESOURCE_ENERGY] = 100;
    creep.room.energyAvailable = creep.room.energyCapacityAvailable;
    let memory = {W15S34: {'W15S34-pioneer-0': {}}};

    pioneering({memory});

    expect(memory.W15S34['W15S34-pioneer-0'].task).toBe('build');
});

test('cleans up dead rooms', () => {
    Game.structures.Spawn1._removeMock();
    delete Game.rooms.W15S34;
    let memory = {W15S34: {'W15S34-pioneer-0': {}, 'W15S34-pioneer-1': {}}};

    pioneering({memory});

    expect(memory.W15S34).toBeUndefined();
});

test('cleans up dead creeps', () => {
    let memory = {W15S34: {'W15S34-pioneer-0': {}, 'W15S34-pioneer-1': {}}};

    pioneering({memory});

    expect(objectSize(memory.W15S34)).toBe(1);
});

test('does not spawn if no spawn', () => {
    require('territory-api').getCoreRooms();
    Game.spawns['W15S34-0']._removeMock();
    let memory = {};

    pioneering({memory});

    expect(memory.W15S34).toBeUndefined();
});
