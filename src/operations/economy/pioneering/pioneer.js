let {tasks} = require('./tasks');

function identifyTask(creep) {
    if (!creep.carry[RESOURCE_ENERGY]) {
        return 'extractEnergy';
    } else if (creep.room.energyAvailable < creep.room.energyCapacityAvailable) {
        return 'distributeEnergy';
    } else if (creep.room.cachedFind(FIND_MY_CONSTRUCTION_SITES).length) {
        return 'build';
    } else {
        return 'upgradeController';
    }
}

function pioneer(roomMemory, creepMemory, creep) {
    creepMemory.task = creepMemory.task || identifyTask(creep);

    tasks[creepMemory.task](roomMemory, creepMemory, creep);
}

module.exports = {pioneer};
