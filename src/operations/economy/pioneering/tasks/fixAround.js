let {findArray} = require('js-utils');

let FIX_MULTIPLIER = REPAIR_POWER / 2;

function findStructureToFix(creep, range = 0) {
    let fixThreshold = creep.getBodyPartCount(WORK) * FIX_MULTIPLIER;
    if (range === 0) {
        return findArray(creep.pos.lookFor(LOOK_STRUCTURES), structure => {
            return structure.structureType !== STRUCTURE_RAMPART && structure.hitsMax - structure.hits > fixThreshold;
        });
    } else {
        let top = Math.max(creep.pos.y - range, 1);
        let left = Math.max(creep.pos.x - range, 1);
        let bottom = Math.min(creep.pos.y + range, 49);
        let right = Math.min(creep.pos.x + range, 49);
        let lookForAtArea = creep.room.lookForAtArea(LOOK_STRUCTURES, top, left, bottom, right, true);
        let found = findArray(lookForAtArea, ({structure}) => {
            return structure.structureType !== STRUCTURE_RAMPART && structure.structureType !== STRUCTURE_WALL &&
                structure.hitsMax - structure.hits > fixThreshold;
        });

        return found && found.structure;
    }
}

function fixAround(creep, range) {
    let structure = findStructureToFix(creep, range);

    if (structure) {
        creep.repair(structure);

        return Math.min((structure.hitsMax - structure.hits) * REPAIR_COST, creep.carry[RESOURCE_ENERGY] || 0, creep.getBodyPartCount(WORK));
    } else {
        return ERR_NOT_FOUND;
    }
}

module.exports = {fixAround};
