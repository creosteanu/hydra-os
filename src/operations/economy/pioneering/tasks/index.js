let {extractEnergy} = require('./extractEnergy');
let {distributeEnergy} = require('./distributeEnergy');
let {upgradeController} = require('./upgradeController');
let {build} = require('./build');

module.exports.tasks = {extractEnergy, distributeEnergy, upgradeController, build};
