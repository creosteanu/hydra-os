let {get, filterArray, findArray, maxArray, minArray, sumObject, findObject} = require('js-utils');

function harvest(creep, creepMemory, target) {
    if (creep.getBodyPartCount(WORK) * HARVEST_POWER >= creep.carryCapacity - (creep.carry[RESOURCE_ENERGY] || 0)) {
        delete creepMemory.task;
        delete creepMemory.targetId;
    }

    return target.energy && creep.harvest(target);
}

function withdraw(target, creep, creepMemory) {
    if (target.store[RESOURCE_ENERGY] >= creep.carryCapacity - (creep.carry[RESOURCE_ENERGY] || 0)) {
        delete creepMemory.task;
    }

    delete creepMemory.targetId;

    return creep.withdraw(target, RESOURCE_ENERGY);
}

function pickUp(target, creep, creepMemory) {
    if (target.amount >= creep.carryCapacity - (creep.carry[RESOURCE_ENERGY] || 0)) {
        delete creepMemory.task;
    }

    delete creepMemory.targetId;

    return creep.pickup(target);
}

function findEnergySource(roomMemory, creepMemory, creep) {
    return minArray(creep.room.cachedFind(FIND_SOURCES), source => {
        return sumObject(roomMemory, creepMemory => {
            return creepMemory.targetId === source.id ? 1 : 0;
        });
    });
}

function getEnergySource(roomMemory, creepMemory, creep) {
    let target = Game.getObjectById(creepMemory.targetId);

    return target && target.energyCapacity ? target : findEnergySource(roomMemory, creepMemory, creep);
}

function getContainers(roomMemory, creep) {
    let containers = filterArray(creep.room.findContainers(), container => {
        let energy = sumObject(roomMemory, creepMemory => {
            return creepMemory.targetId === container.id ? -creep.carryCapacity : 0;
        }, container.store[RESOURCE_ENERGY] || 0);

        return energy >= creep.carryCapacity / 2;
    });

    if (get(creep.room.storage, ['store', RESOURCE_ENERGY]) > creep.carryCapacity) {
        containers.push(creep.room.storage);
    }

    if (get(creep.room.terminal, ['store', RESOURCE_ENERGY]) > creep.carryCapacity) {
        containers.push(creep.room.terminal);
    }

    return containers;
}

function findContainer(roomMemory, creep) {
    return minArray(getContainers(roomMemory, creep), container => creep.pos.getRange(container.pos));
}

function findTomb(roomMemory, creep) {
    return findArray(creep.room.cachedFind(FIND_TOMBSTONES), tomb => {
        return tomb.store[RESOURCE_ENERGY] >= creep.carryCapacity / 2 && !findObject(roomMemory, creepMemory => creepMemory.targetId === tomb.id);
    });
}

function findEnergyDrops(roomMemory, creep) {
    return filterArray(creep.room.cachedFind(FIND_DROPPED_RESOURCES), resource => {
        if (resource.resourceType !== RESOURCE_ENERGY) {
            return false;
        }

        let energy = sumObject(roomMemory, creepMemory => {
            return creepMemory.targetId === resource.id ? -creep.carryCapacity : 0;
        }, resource.amount);

        return energy >= creep.carryCapacity / 2;
    });
}

function findEnergyDrop(roomMemory, creep) {
    return maxArray(findEnergyDrops(roomMemory, creep), resource => resource.amount);
}

function getMemorizedTarget(creepMemory, creep) {
    let target = Game.getObjectById(creepMemory.targetId);
    let energy = get(target, ['amount']) || get(target, ['store', RESOURCE_ENERGY]);

    return energy >= creep.carryCapacity / 2 ? target : false;
}

function getTarget(roomMemory, creepMemory, creep) {
    return getMemorizedTarget(creepMemory, creep) ||
        findEnergyDrop(roomMemory, creep) || findTomb(roomMemory, creep) || findContainer(roomMemory, creep) ||
        getEnergySource(roomMemory, creepMemory, creep);
}

function extractEnergy(roomMemory, creepMemory, creep) {
    let target = getTarget(roomMemory, creepMemory, creep);
    if (!target) {
        return undefined;
    }

    creepMemory.targetId = target.id;

    if (!creep.pos.nearTo(target.pos)) {
        return creep.moveTo(target);
    } else if (target.resourceType) {
        return pickUp(target, creep, creepMemory);
    } else if (target.store) {
        return withdraw(target, creep, creepMemory);
    } else {
        return harvest(creep, creepMemory, target);
    }
}

module.exports = {extractEnergy};
