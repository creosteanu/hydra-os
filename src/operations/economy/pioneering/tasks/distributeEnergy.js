let {filterArray, maxArray, sumObject} = require('js-utils');

let {fixAround} = require('./fixAround');

function moveToTarget(creep, target, creepMemory) {
    creep.moveTo(target);
    let energySpent = fixAround(creep, 3);
    if (energySpent === creep.carry[RESOURCE_ENERGY]) {
        delete creepMemory.task;
        delete creepMemory.targetId;
    }
}

function getDistributionTarget(roomMemory, creepMemory, creep) {
    let target = Game.getObjectById(creepMemory.targetId);
    if (target && target.energy < target.energyCapacity) {
        return target;
    } else {
        let energyConsumers = creep.room.findMyStructures(STRUCTURE_EXTENSION)
            .concat(creep.room.findMyStructures(STRUCTURE_SPAWN))
            .concat(creep.room.findMyStructures(STRUCTURE_TOWER));
        energyConsumers = filterArray(energyConsumers, structure => structure.energy < structure.energyCapacity);

        return maxArray(energyConsumers, structure => {
            let energyDemand = structure.energyCapacity - structure.energy;

            return sumObject(roomMemory, creepMemory => {
                return creepMemory.targetId === structure.id ? -creep.carryCapacity : 0;
            }, energyDemand);
        });
    }
}

function transferEnergy(creepMemory, creep, target) {
    if (target.energyCapacity - target.energy >= (creep.carry[RESOURCE_ENERGY] || 0)) {
        delete creepMemory.task;
    }

    delete creepMemory.targetId;
    creep.transfer(target, RESOURCE_ENERGY);
}

function distributeEnergy(roomMemory, creepMemory, creep) {
    let target = getDistributionTarget(roomMemory, creepMemory, creep);
    if (!target) {
        delete creepMemory.task;
        delete creepMemory.targetId;

        return;
    }

    creepMemory.targetId = target.id;

    if (creep.pos.nearTo(target.pos)) {
        transferEnergy(creepMemory, creep, target);
    } else {
        moveToTarget(creep, target, creepMemory);
    }
}

module.exports = {distributeEnergy};
