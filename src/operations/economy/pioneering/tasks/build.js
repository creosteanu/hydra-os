let {minArray} = require('js-utils');

let {fixAround} = require('./fixAround');

function goToConstruction(creep, target, creepMemory) {
    creep.moveTo(target);
    let energySpent = fixAround(creep, 3);
    if (energySpent === creep.carry[RESOURCE_ENERGY]) {
        delete creepMemory.task;
        delete creepMemory.targetId;
    }
}

function buildConstructionSite(creepMemory, creep, target) {
    if ((creep.carry[RESOURCE_ENERGY] || 0) <= creep.getBodyPartCount(WORK) * BUILD_POWER) {
        delete creepMemory.task;
        delete creepMemory.targetId;
    }

    creep.build(target);
}

function getNearestConstruction(creep, room) {
    return minArray(room.cachedFind(FIND_MY_CONSTRUCTION_SITES), construction => creep.pos.getRange(construction.pos));
}

function build(roomMemory, creepMemory, creep) {
    let target = Game.getObjectById(creepMemory.targetId) || getNearestConstruction(creep, creep.room);
    if (!target) {
        delete creepMemory.task;
        delete creepMemory.targetId;

        return;
    }

    creepMemory.targetId = target.id;

    if (creep.pos.inRange(target.pos, 3)) {
        buildConstructionSite(creepMemory, creep, target);
    } else {
        goToConstruction(creep, target, creepMemory);
    }
}

module.exports = {build};
