let distributeEnergy;
let creep;
let creepMemory;

beforeEach(() => {
    distributeEnergy = require('../distributeEnergy').distributeEnergy;

    creep = new Creep({
        pos:   new RoomPosition(1, 3, 'W15S34'),
        name:  'W15S34-pioneer-0',
        body:  [{type: MOVE, hits: 100}, {type: CARRY, hits: 100}, {type: CARRY, hits: 100}, {type: WORK, hits: 100}],
        carry: {[RESOURCE_ENERGY]: 1}
    });
    creepMemory = {task: 'distributeEnergy'};
});

test('deletes task if no target', () => {
    creep.room.find = () => [];

    distributeEnergy({}, creepMemory, creep);

    expect(creepMemory.task).toBeUndefined();
});

test('moves towards distribution target if not near it', () => {
    let extension = new StructureExtension({id: 'extensionId', pos: new RoomPosition(4, 5, 'W15S34')});
    new StructureExtension({id: 'extension2Id', pos: new RoomPosition(5, 5, 'W15S34')});
    new StructureRampart({pos: new RoomPosition(6, 6, 'W15S34')});
    creep.moveTo = jest.fn();

    distributeEnergy({creep2: {targetId: 'extension2Id'}}, creepMemory, creep);

    expect(creep.moveTo).toHaveBeenCalledWith(extension);
    expect(creepMemory.targetId).toBe('extensionId');
});

test('repairs while moving', () => {
    new StructureExtension({id: 'extensionId', pos: new RoomPosition(4, 5, 'W15S34')});
    new StructureRoad({pos: new RoomPosition(1, 2, 'W15S34'), hits: 100, hitsMax: 5000});
    creep.repair = jest.fn();

    distributeEnergy({}, creepMemory, creep);

    expect(creep.repair).toHaveBeenCalled();
    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});

test('transfers energy if near Structure', () => {
    new StructureExtension({id: 'extensionId', pos: new RoomPosition(2, 3, 'W15S34')});
    creep.transfer = jest.fn();

    distributeEnergy({}, creepMemory, creep);

    expect(creep.transfer).toHaveBeenCalled();
    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});

test('picks another target if memorized structure is full', () => {
    new StructureExtension({id: 'extensionId', energy: 50, pos: new RoomPosition(4, 5, 'W15S34')});
    new StructureExtension({id: 'extension2Id', pos: new RoomPosition(4, 5, 'W15S34')});

    creepMemory.targetId = 'extensionId';
    distributeEnergy({}, creepMemory, creep);

    expect(creepMemory.targetId).toBe('extension2Id');
});

test('does not use room find with valid memorized structure', () => {
    new StructureExtension({id: 'extensionId', pos: new RoomPosition(4, 5, 'W15S34')});
    let findSpy = jest.spyOn(creep.room, 'findMyStructures');

    creepMemory.targetId = 'extensionId';
    distributeEnergy({}, creepMemory, creep);

    expect(findSpy).not.toHaveBeenCalled();
});
