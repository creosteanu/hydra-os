/* eslint-disable max-lines */

let extractEnergy;
let creep;
let creepMemory;

beforeEach(() => {
    extractEnergy = require('../extractEnergy').extractEnergy;

    creep = new Creep({
        pos:   new RoomPosition(1, 3, 'W15S34'),
        name:  'W15S34-pioneer-0',
        body:  [{type: MOVE, hits: 100}, {type: CARRY, hits: 100}, {type: CARRY, hits: 100}, {type: WORK, hits: 100}],
        carry: {[RESOURCE_ENERGY]: 1}
    });
    creepMemory = {task: 'extractEnergy'};
});

test('identifies dropped energy and moves towards it', () => {
    let resource = new Resource({id: 'resourceId', amount: 120, pos: new RoomPosition(2, 5, 'W15S34'), resourceType: RESOURCE_ENERGY});
    new Resource({id: 'resourceSmall', amount: 100, pos: new RoomPosition(4, 5, 'W15S34'), resourceType: RESOURCE_ENERGY});
    new Resource({id: 'resourceLarge', amount: 130, pos: new RoomPosition(3, 5, 'W15S34'), resourceType: RESOURCE_ENERGY});
    new Resource({id: 'notEnergyResource', amount: 130, pos: new RoomPosition(3, 5, 'W15S34'), resourceType: RESOURCE_CATALYST});
    creep.moveTo = jest.fn();

    extractEnergy({creep2: {targetId: 'resourceLarge'}}, creepMemory, creep);

    expect(creepMemory.targetId).toBe('resourceId');
    expect(creep.moveTo).toHaveBeenCalledWith(resource);
});

test('remembers dropped energy and picks it up', () => {
    new Resource({id: 'resourceId', amount: 200, pos: new RoomPosition(1, 4, 'W15S34'), resourceType: RESOURCE_ENERGY});
    creep.pickup = jest.fn();
    creepMemory.targetId = 'resourceId';

    extractEnergy({}, creepMemory, creep);

    expect(creep.pickup).toHaveBeenCalled();
    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});

test('does nothing if no valid energy source', () => {
    creep.moveTo = jest.fn();
    creep.pickup = jest.fn();
    creep.harvest = jest.fn();

    extractEnergy({}, {}, creep);

    expect(creep.pickup).not.toHaveBeenCalled();
    expect(creep.harvest).not.toHaveBeenCalled();
    expect(creep.moveTo).not.toHaveBeenCalled();
});

test('identifies energy source and moves towards it', () => {
    let source = new Source({energy: 100, pos: new RoomPosition(2, 5, 'W15S34'), id: 'sourceId'});
    creep.moveTo = jest.fn();

    extractEnergy({}, creepMemory, creep);

    expect(creepMemory.targetId).toBe('sourceId');
    expect(creep.moveTo).toHaveBeenCalledWith(source);
});

test('picks source with least creeps assigned', () => {
    new Source({energy: 100, pos: new RoomPosition(2, 5, 'W15S34'), id: 'sourceId'});
    new Source({energy: 100, pos: new RoomPosition(5, 5, 'W15S34'), id: 'source2Id'});

    extractEnergy({creep2: {targetId: 'sourceId'}}, creepMemory, creep);

    expect(creepMemory.targetId).toBe('source2Id');
});

test('remembers source and harvests it', () => {
    new Source({energy: 100, pos: new RoomPosition(1, 4, 'W15S34'), id: 'sourceId'});
    creep.harvest = jest.fn();
    creep.carry[RESOURCE_ENERGY] = 99;
    creepMemory.targetId = 'sourceId';

    extractEnergy({}, creepMemory, creep);

    expect(creep.harvest).toHaveBeenCalled();
    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});

test('picks unassigned tomb', () => {
    let tombstone = new Tombstone({id: 'tombId', pos: new RoomPosition(1, 4, 'W15S34'), store: {energy: 100}});
    new Tombstone({id: 'smallTomb', pos: new RoomPosition(1, 6, 'W15S34'), store: {energy: 20}});
    new Tombstone({id: 'assignedTomb', pos: new RoomPosition(1, 5, 'W15S34'), store: {energy: 100}});
    creep.withdraw = jest.fn();

    extractEnergy({creep2: {targetId: 'assignedTomb'}}, {}, creep);

    expect(creep.withdraw).toHaveBeenCalledWith(tombstone, RESOURCE_ENERGY);
});

test('picks nearest container', () => {
    let container = new StructureContainer({id: 'containerId', pos: new RoomPosition(1, 4, 'W15S34'), store: {energy: 100}});
    new StructureContainer({id: 'furtherContainer', pos: new RoomPosition(1, 5, 'W15S34'), store: {energy: 100}});
    new StructureExtension({id: 'notContainer', pos: new RoomPosition(1, 6, 'W15S34')});
    new StructureContainer({id: 'largeContainer', pos: new RoomPosition(1, 5, 'W15S34'), store: {energy: 130}});
    creep.withdraw = jest.fn();

    extractEnergy({creep2: {targetId: 'container4Id'}}, {}, creep);

    expect(creep.withdraw).toHaveBeenCalledWith(container, RESOURCE_ENERGY);
});

test('takes into account storage', () => {
    new StructureStorage({store: {energy: 200}, pos: new RoomPosition(2, 4, 'W15S34')});
    creep.withdraw = jest.fn();

    extractEnergy({}, {}, creep);

    expect(creep.withdraw).toHaveBeenCalledWith(creep.room.storage, RESOURCE_ENERGY);
});

test('takes into account terminal', () => {
    new StructureTerminal({store: {energy: 200}, pos: new RoomPosition(2, 4, 'W15S34')});
    creep.withdraw = jest.fn();

    extractEnergy({}, {}, creep);

    expect(creep.withdraw).toHaveBeenCalledWith(creep.room.terminal, RESOURCE_ENERGY);
});
