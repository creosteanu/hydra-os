let {upgradeController} = require('../upgradeController');

let creep;
let creepMemory;
beforeEach(() => {
    creep = new Creep({
        pos:   new RoomPosition(1, 3, 'W15S34'),
        name:  'W15S34-pioneer-0',
        body:  [{type: MOVE, hits: 100}, {type: CARRY, hits: 100}, {type: CARRY, hits: 100}, {type: WORK, hits: 100}],
        carry: {[RESOURCE_ENERGY]: 1}
    });
    creepMemory = {task: 'upgradeController'};
});

test('moves towards controller when not in range 3', () => {
    creep.moveTo = jest.fn();

    upgradeController({}, {}, creep);

    expect(creep.moveTo).toHaveBeenCalledWith(creep.room.controller);
});

test('repairs while moving', () => {
    new StructureRoad({pos: new RoomPosition(1, 2, 'W15S34'), hits: 100, hitsMax: 5000});
    creep.repair = jest.fn();

    upgradeController({}, creepMemory, creep);

    expect(creep.repair).toHaveBeenCalled();
    expect(creepMemory.task).toBeUndefined();
});

test('upgrades controller when in range 3', () => {
    creep.pos = new RoomPosition(21, 21, 'W15S34');
    creep.upgradeController = jest.fn();

    upgradeController({}, creepMemory, creep);

    expect(creep.upgradeController).toHaveBeenCalled();
    expect(creepMemory.task).toBeUndefined();
});
