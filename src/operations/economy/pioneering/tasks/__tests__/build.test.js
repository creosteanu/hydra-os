let {build} = require('../build');

let creep;
beforeEach(() => {
    creep = new Creep({
        pos:   new RoomPosition(1, 3, 'W15S34'),
        name:  'W15S34-pioneer-0',
        body:  [{type: MOVE, hits: 100}, {type: CARRY, hits: 100}, {type: CARRY, hits: 100}, {type: WORK, hits: 100}],
        carry: {[RESOURCE_ENERGY]: 1}
    });
});

test('moves towards constructionSite when not in range 3', () => {
    let constructionSite = new ConstructionSite({pos: new RoomPosition(5, 6, 'W15S34')});
    creep.moveTo = jest.fn();

    build({}, {}, creep);

    expect(creep.moveTo).toHaveBeenCalledWith(constructionSite);
});

test('builds construction site when in range 3', () => {
    new ConstructionSite({pos: new RoomPosition(5, 6, 'W15S34')});
    creep.pos = new RoomPosition(4, 6, 'W15S34');
    creep.build = jest.fn();

    let creepMemory = {task: 'build'};
    build({}, creepMemory, creep);

    expect(creep.build).toHaveBeenCalled();
    expect(creepMemory.task).toBeUndefined();
});

test('removes task if no target', () => {
    let creepMemory = {task: 'build'};
    build({}, creepMemory, creep);

    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});

test('repairs nearby structures whilst out of range', () => {
    new ConstructionSite({pos: new RoomPosition(5, 6, 'W15S34')});
    new StructureRoad({pos: new RoomPosition(1, 2, 'W15S34'), hits: 100, hitsMax: 5000});
    creep.repair = jest.fn();

    let creepMemory = {task: 'build'};
    build({}, creepMemory, creep);

    expect(creep.repair).toHaveBeenCalled();
    expect(creepMemory.targetId).toBeUndefined();
    expect(creepMemory.task).toBeUndefined();
});
