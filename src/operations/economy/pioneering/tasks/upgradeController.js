let {fixAround} = require('./fixAround');

function upgradeController(roomMemory, creepMemory, creep) {
    if (creep.pos.inRange(creep.room.controller.pos, 3)) {
        if (creep.getBodyPartCount(WORK) >= (creep.carry[RESOURCE_ENERGY] || 0)) {
            delete creepMemory.task;
        }

        creep.upgradeController(creep.room.controller);
    } else {
        creep.moveTo(creep.room.controller);
        let energySpent = fixAround(creep, 3);
        if (energySpent === creep.carry[RESOURCE_ENERGY]) {
            delete creepMemory.task;
        }
    }
}

module.exports = {upgradeController};
