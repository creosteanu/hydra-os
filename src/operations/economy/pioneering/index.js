let {set, forObject, objectSize} = require('js-utils');
let {executeTertiaryProcess} = require('screeps-utils');

let {getCoreRooms} = require('territory-api');
let {createCreepBody, generateCreepName, scheduleSpawn} = require('spawning-api');
let {creepExists, callIfReady} = require('creep-api');

let {pioneer} = require('./pioneer');

function schedulePioneer(memory, room, energy) {
    let creepName = generateCreepName(`${room.name}-pioneer`);
    let body = createCreepBody([MOVE, WORK, CARRY], energy);

    scheduleSpawn({body, creepName, roomName: room.name, priority: 100});

    set(memory, [room.name, creepName], {});
}

let BASE_PIONEER_TARGET = 3;

function insufficientPioneers(memory, room) {
    let sourceCount = room.cachedFind(FIND_SOURCES).length;

    return objectSize(memory[room.name]) < BASE_PIONEER_TARGET * sourceCount;
}

function schedulePioneers(memory) {
    forObject(getCoreRooms(), room => {
        if (!executeTertiaryProcess(1, room.controller.x) || !room.findMyStructures(STRUCTURE_SPAWN).length) {
            return;
        }

        if (insufficientPioneers(memory, room)) {
            schedulePioneer(memory, room, room.energyCapacityAvailable);
        }
    });
}

function processRooms(memory) {
    forObject(memory, (roomMemory, roomName) => {
        if (Game.rooms[roomName]) {
            forObject(roomMemory, (creepMemory, creepName) => {
                if (creepExists(creepName)) {
                    callIfReady(creepName, pioneer, [roomMemory, creepMemory, Game.creeps[creepName]]);
                } else {
                    delete roomMemory[creepName];
                }
            });

            if (objectSize(roomMemory) === 0) {
                delete memory[roomName];
            }
        } else {
            delete memory[roomName];
        }
    });
}

function pioneering({memory}) {
    processRooms(memory);
    schedulePioneers(memory);
}

pioneering.core = true;

module.exports = {pioneering};
