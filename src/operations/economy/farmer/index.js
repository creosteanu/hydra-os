let {forArray, filterArray} = require('js-utils');

let {creepExists, callIfReady} = require('creep-api');

let {spawnFarmers} = require('./farmerSpawn');
let {farm} = require('./farm');

function farmer({id, memory}) {
    memory.creeps = filterArray(memory.creeps, creepExists);

    if (memory.creeps.length > 0) {
        forArray(memory.creeps, creepName => callIfReady(creepName, farm, [id, memory, creepName]));
    } else {
        spawnFarmers(id, memory);
    }
}

module.exports = {farmer};
