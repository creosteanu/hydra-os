let {FARMER_BODY, FARMER_BODY_COST} = require('hydra-constants');

let {createCreepBody, generateCreepName, scheduleSpawn} = require('spawning-api');

function getBody(memory, room) {
    if (room.energyCapacityAvailable < FARMER_BODY_COST) {
        return createCreepBody([WORK, WORK, MOVE], room.energyCapacityAvailable);
    } else {
        return FARMER_BODY;
    }
}

function spawnFarmers(id, memory) {
    let room = Game.rooms[memory.origin];
    if (!room) {
        throw 'spawnRoomInvisible';
    }

    let body = getBody(memory, room);
    let creepName = generateCreepName(id);

    scheduleSpawn({body, creepName, roomName: room.name, priority: 50});

    memory.creeps.push(creepName);
}

module.exports = {spawnFarmers};
