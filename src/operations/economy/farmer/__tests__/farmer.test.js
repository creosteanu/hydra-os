let {spawnScheduled} = require('spawning-api');
let {farmer} = require('../index');

let id;
let memory;
beforeEach(() => {
    id = '1,2,W15S34-farm';
    memory = {origin: 'W15S34', sourceId: 'someId'};
});

test('is part of operations object', () => {
    expect(require('../../../index').operationsLogic.farmer).toBeDefined();
});

test('schedules creep spawn', () => {
    farmer({id, memory});

    expect(memory.creeps).toHaveLength(1);
    expect(spawnScheduled(`${id}-0`)).toBeTruthy();
    expect(Memory.operations.spawning).toMatchSnapshot();
});

test('schedules small creep spawn when insufficient room energy', () => {
    Game.rooms.W15S34.energyCapacityAvailable = 500;
    farmer({id, memory});

    expect(memory.creeps).toHaveLength(1);
    expect(spawnScheduled(`${id}-0`)).toBeTruthy();
    expect(Memory.operations.spawning).toMatchSnapshot();
});

test('schedules creep with iterative name', () => {
    new Creep({name: `${id}-0`});
    farmer({id, memory});

    expect(memory.creeps).toHaveLength(1);
    expect(spawnScheduled(`${id}-1`)).toBeTruthy();
});

test('schedules only 1 creep spawn', () => {
    farmer({id, memory});
    farmer({id, memory});

    expect(memory.creeps).toHaveLength(1);
});

test('sets farm position', () => {
    let creepName = `${id}-0`;
    new Creep({name: creepName, pos: new RoomPosition(4, 5, 'W15S34')});
    PathFinder.search = () => ({path: [new RoomPosition(3, 4, 'W15S34'), new RoomPosition(2, 3, 'W15S34'), new RoomPosition(1, 3, 'W15S34')]});
    let memory = {origin: 'W15S34', creeps: [creepName]};

    farmer({id, memory});

    expect(memory.farmPosition).toBe('1,3,W15S34');
});

test('does not set farm position when path is incomplete', () => {
    let creepName = `${id}-0`;
    new Creep({name: creepName, pos: new RoomPosition(4, 5, 'W15S34')});
    PathFinder.search = () => ({path: [new RoomPosition(3, 4, 'W15S34'), new RoomPosition(2, 3, 'W15S34')], incomplete: true});
    let memory = {origin: 'W15S34', creeps: [creepName]};

    farmer({id, memory});

    expect(memory.farmPosition).toBeUndefined();
});

test('throws error for invisible origin', () => {
    new Creep({name: `${id}-0`});
    memory.origin = 'W15S37';

    expect(() => farmer({id, memory})).toThrow('spawnRoomInvisible');
});
