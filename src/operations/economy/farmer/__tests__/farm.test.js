let farm;
let id = '1,2,W15S34-farm';
let creepName = `${id}-0`;
let creep;
let memory;

beforeEach(() => {
    farm = require('../farm').farm;
    creep = new Creep({name: creepName, pos: new RoomPosition(1, 3, 'W15S34')});
    memory = {origin: 'W15S34', farmPosition: '1,3,W15S34', creeps: [creepName]};
    new Source({id: 'someId', pos: new RoomPosition(1, 2, 'W15S34')});
});

test('harvests when in farm position', () => {
    creep.harvest = jest.fn();

    farm(id, memory, creepName);

    expect(creep.harvest.mock.calls).toMatchSnapshot();
    expect(memory).toMatchSnapshot();
});

test('sets nextSpawnTime when farming', () => {
    creep.harvest = jest.fn();
    creep.ticksToLive = 1000;

    farm(id, memory, creepName);

    expect(memory.nextSpawnTime).toBe(Game.time + 497);
});

test('does not overwrite nextSpawnTime', () => {
    creep.ticksToLive = 1000;
    memory.nextSpawnTime = 400;

    farm(id, memory, creepName);

    expect(memory.nextSpawnTime).toBe(400);
});

test('schedules spawn when Game.time passes nextSpawnTime', () => {
    memory.nextSpawnTime = -1;

    farm(id, memory, creepName);

    expect(memory.nextSpawnTime).toBeUndefined();
    expect(memory.creeps).toHaveLength(2);
});

test('does not set nextSpawnTime when 2 creeps exist', () => {
    memory.creeps.push(`${id}-1`);

    farm(id, memory, creepName);

    expect(memory.nextSpawnTime).toBeUndefined();
});

test('moves when not in position', () => {
    creep.pos = new RoomPosition(4, 5, 'W15S34');
    creep.moveTo = jest.fn();

    farm(id, memory, creepName);

    expect(creep.moveTo).toHaveBeenCalledWith(new RoomPosition(1, 3, 'W15S34'));
});
