let {spawnFarmers} = require('./farmerSpawn');

function spawnNext(id, memory, creep) {
    if (memory.creeps.length >= 2) {
        return;
    }

    if (memory.nextSpawnTime === undefined) {
        memory.nextSpawnTime = Game.time + 2 * creep.ticksToLive - CREEP_LIFE_TIME - CREEP_SPAWN_TIME;
    } else if (memory.nextSpawnTime <= Game.time) {
        spawnFarmers(id, memory);
        delete memory.nextSpawnTime;
    }
}

function setSourceId(memory, id) {
    memory.sourceId = memory.sourceId || RoomPosition.fromLabel(id.split('-')[0]).lookFor(LOOK_SOURCES)[0].id;
}

function getFarmPosition(id, memory, creepName) {
    if (memory.farmPosition) {
        return RoomPosition.fromLabel(memory.farmPosition);
    }

    let sourcePos = RoomPosition.fromLabel(id.split('-')[0]);
    let creepPos = Game.creeps[creepName].pos;

    let {incomplete, path} = PathFinder.search(creepPos, {pos: sourcePos, range: 1});

    if (incomplete) {
        return sourcePos;
    }

    let farmPosition = creepPos.nearTo(sourcePos) ? creepPos : path[path.length - 1];

    memory.farmPosition = farmPosition.label();

    return farmPosition;
}

function farm(id, memory, creepName) {
    let creep = Game.creeps[creepName];
    let farmPosition = getFarmPosition(id, memory, creepName);
    if (creep.pos.equals(farmPosition)) {
        setSourceId(memory, id);

        creep.harvest(Game.getObjectById(memory.sourceId));

        spawnNext(id, memory, creep);
    } else {
        creep.moveTo(farmPosition);
    }
}

module.exports = {farm};
