let {forObject, get, set, forArray} = require('js-utils');
let {executeTertiaryProcess} = require('screeps-utils');

let {getCoreRooms} = require('territory-api');
let {farm, isFarmed} = require('farmer-api');

function initRoomFarming(memory, room) {
    set(memory, ['assignedSources', room.name], 0);

    forArray(room.cachedFind(FIND_SOURCES), source => {
        let sourcePosLabel = source.pos.label();
        if (!isFarmed(sourcePosLabel)) {
            memory.assignedSources[room.name]++;
            farm(sourcePosLabel, room.name);
        }
    });
}

function farming({memory}) {
    if (!executeTertiaryProcess()) {
        return;
    }

    forObject(getCoreRooms(), room => {
        if (get(memory, ['assignedSources', room.name]) === undefined) {
            initRoomFarming(memory, room);
        }
    });
}

farming.core = true;

module.exports = {farming};
