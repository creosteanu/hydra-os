let farming;

beforeEach(() => {
    farming = require('../index').farming;
});

test('is core', () => {
    expect(farming.core).toBeTruthy();
});

test('is part of operations object', () => {
    let {operationsLogic} = require('../../../index');

    expect(operationsLogic.farming).toBeDefined();
});

test('executes as tertiary process', () => {
    Game.cpu.bucket = 4900;

    let memory = {};

    farming({memory});

    expect(memory.assignedSources).toBeUndefined();
});

test('initializes assignedSources for owned room', () => {
    Game.rooms.W15S34.energyCapacityAvailable = 700;
    let memory = {};

    farming({memory});

    expect(memory.assignedSources.W15S34).toBe(0);
});

test('assigns sources in room', () => {
    Memory.operations = {};
    new Source({pos: new RoomPosition(1, 2, 'W15S34')});

    let memory = {};

    farming({memory});

    expect(memory.assignedSources.W15S34).toBe(1);
    expect(Memory.operations['1,2,W15S34-farm']).toBeDefined();
});
