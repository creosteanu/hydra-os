let {createCreepBody, scheduleSpawn} = require('spawning-api');

test('createCreepBody provides valid body', () => {
    expect(createCreepBody([WORK, WORK, MOVE], 500)).toMatchSnapshot();
});

test('scheduleSpawn with no rooms will throw error', () => {
    expect(() => scheduleSpawn({})).toThrow(ERR_INVALID_ARGS.toString());
});

test('scheduleSpawn with no body will throw error', () => {
    expect(() => scheduleSpawn({roomName: 'room1'})).toThrow(ERR_NO_BODYPART.toString());
});
