let spawning;
let aCreep;
let memory;

beforeEach(() => {
    spawning = require('../spawning').spawning;
    aCreep = {time: 0, roomName: 'W15S34', energy: 100, body: [MOVE]};
    memory = {scheduledSpawn: {aCreep}};
});

test('is core', () => {
    expect(spawning.core).toBeTruthy();
});

test('is part of operations object', () => {
    let {operationsLogic} = require('../index');

    expect(operationsLogic.spawning).toBeDefined();
});

test('spawns creep', () => {
    let spawnSpy = jest.spyOn(Game.spawns['W15S34-0'], 'spawnCreep');

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.aCreep).toBeUndefined();
    expect(memory.shouldSpawn.aCreep).toEqual(aCreep);
    expect(spawnSpy.mock.calls).toMatchSnapshot();
});

test('uses another spawn if first one is busy', () => {
    Game.spawns['W15S34-0'].spawning = true;
    new StructureSpawn({name: 'W15S34-1', id: 'Spawn2', pos: new RoomPosition(10, 11, 'W15S34')});

    let spawnSpy = jest.spyOn(Game.spawns['W15S34-1'], 'spawnCreep');

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.aCreep).toBeUndefined();
    expect(memory.shouldSpawn.aCreep).toEqual(aCreep);
    expect(spawnSpy.mock.calls).toMatchSnapshot();
});

test('prioritizes by priority property', () => {
    memory.scheduledSpawn.priorityCreep = Object.assign(Object.create(aCreep), {priority: 1});

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.priorityCreep).toBeUndefined();
    expect(memory.scheduledSpawn.aCreep).toBeDefined();
    expect(memory.shouldSpawn.priorityCreep).toBeDefined();
});

test('does not spawn if no available spawn', () => {
    Game.spawns['W15S34-0'].spawning = true;

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.aCreep).toBeDefined();
});

test('skips spawn if exceeds maximum spawn capacity', () => {
    memory.scheduledSpawn.aCreep.energy = 20000;
    memory.scheduledSpawn.validCreep = Object.assign(Object.create(aCreep), {energy: 500});

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.aCreep).toBeDefined();
    expect(memory.scheduledSpawn.validCreep).toBeUndefined();
});

test('does not spawn if exceeds available energy', () => {
    memory.scheduledSpawn.aCreep.energy = 500;
    memory.scheduledSpawn.invalidCreep = Object.assign(Object.create(aCreep), {energy: 1500});

    spawning({id: 'spawning', memory});

    expect(memory.scheduledSpawn.aCreep).toBeDefined();
    expect(memory.scheduledSpawn.invalidCreep).toBeDefined();
});

test('retries spawn if it failed', () => {
    let consoleSpy = jest.spyOn(console, 'log').mockReturnValue();
    let spawnSpy = jest.spyOn(Game.spawns['W15S34-0'], 'spawnCreep');

    spawning({id: 'spawning', memory: {shouldSpawn: {aCreep}}});

    expect(consoleSpy.mock.calls).toMatchSnapshot();
    expect(spawnSpy.mock.calls).toMatchSnapshot();
});
