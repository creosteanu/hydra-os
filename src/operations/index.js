let {farmer, farming, pioneering} = require('./economy');
let {spawning} = require('./spawning');

let operationsLogic = {farmer, farming, pioneering, spawning};

module.exports = {operationsLogic};
