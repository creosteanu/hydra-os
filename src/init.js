let {forObject} = require('js-utils');
let {version} = require('../package');

function initOperation(Memory, logic, name) {
    if (logic.core && !Memory.operations[name]) {
        Memory.operations[name] = logic.init ? logic.init() : {};
    }
}

function initOperations(Memory, operationsLogic) {
    Memory.operations = Memory.operations || {};

    forObject(operationsLogic, initOperation.bind(null, Memory));
}

function init(Memory, operationsLogic) {
    if (version !== Memory.version) {
        Memory.version = version;
        initOperations(Memory, operationsLogic);
    }
}

module.exports = {init};
