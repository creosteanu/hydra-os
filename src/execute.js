let {forObject, safeCall} = require('js-utils');

function execute(Memory, operationsLogic) {
    forObject(Memory.operations, (memory, id) => {
        safeCall(operationsLogic[memory.name || id], [{id, memory}]);
    });
}

module.exports = {execute};
