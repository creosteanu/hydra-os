let {resetCache} = require('cache');

require('./screepsClassExtensions');

let {operationsLogic} = require('./operations');
let {init} = require('./init');
let {execute} = require('./execute');

function loop() {
    resetCache();
    init(Memory, operationsLogic);
    execute(Memory, operationsLogic);
}

module.exports = {loop};
