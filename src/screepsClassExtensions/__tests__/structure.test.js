test('valid toString output', () => {
    expect(new Structure({id: 'SomeStructure', pos: new RoomPosition(1, 1, 'W15S34')}).toString()).toMatchSnapshot();
});
