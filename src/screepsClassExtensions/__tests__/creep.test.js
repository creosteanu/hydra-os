let creep;

beforeEach(() => {
    creep = new Creep({pos: new RoomPosition(1, 2, 'W1S1'), id: 'creepId'});
});

test('valid toString output', () => {
    expect(creep.toString()).toMatchSnapshot();
});

test('active body parts ignores dead parts', () => {
    creep.body = [{hits: 0, type: MOVE}, {hits: 100, type: ATTACK}];

    expect(creep.getBodyPartCount(MOVE)).toBe(0);
    expect(creep.getBodyPartCount(ATTACK)).toBe(1);
});
