test('fromLabel', () => {
    let position = RoomPosition.fromLabel('1,2,W1S1');

    expect(position).toBeInstanceOf(RoomPosition);
    expect(position.x).toBe(1);
    expect(position.y).toBe(2);
    expect(position.roomName).toBe('W1S1');
});

test('label', () => {
    let position = new RoomPosition(1, 2, 'W1S1');

    expect(position.label()).toEqual('1,2,W1S1');
});

test('position with impassible terrain is not walkable', () => {
    let position = new RoomPosition(1, 2, 'W1S1');
    position.lookFor = type => { return type === LOOK_TERRAIN ? ['wall'] : []; };

    expect(position.isWalkable()).toBeFalsy();
});

test('position with blocking structures is not walkable', () => {
    let pos = new RoomPosition(1, 2, 'W15S34');
    new StructureExtension({pos});

    expect(pos.isWalkable()).toBeFalsy();
});

test('position with blocking construction site is not walkable', () => {
    let pos = new RoomPosition(1, 2, 'W15S34');
    new ConstructionSite({pos, structureType: STRUCTURE_SPAWN});

    expect(pos.isWalkable()).toBeFalsy();
});

test('getDirection returns 0 if different rooms', () => {
    let start = new RoomPosition(1, 2, 'W15S34');
    let end = new RoomPosition(2, 3, 'W4S4');

    expect(start.getDirection(end)).toBe(0);
});
