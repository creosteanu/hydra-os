test('gets distance between rooms', () => {
    expect(Room.getDistance('W15S34', 'W14S34')).toBe(1);
    expect(Room.getDistance('W15S34', 'W13S34')).toBe(2);
    expect(Room.getDistance('W1S1', 'E1S1')).toBe(3);
    expect(Room.getDistance('W1S1', 'N1S1')).toBe(3);
});

test('gets continuous distance', () => {
    Game.map.getWorldSize = () => 40;
    expect(Room.getDistance('W15S1', 'E15S1')).toBe(31);
    expect(Room.getDistance('W15S1', 'E15S1', true)).toBe(9);
});
