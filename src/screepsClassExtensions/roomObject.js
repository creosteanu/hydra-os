function accessibleRampart(structure) {
    return structure.structureType === STRUCTURE_RAMPART && (structure.my || structure.isPublic);
}

function notWalkableStructure(structure) {
    return structure.structureType !== STRUCTURE_CONTAINER && structure.structureType !== STRUCTURE_ROAD && structure.structureType !== STRUCTURE_PORTAL;
}

RoomObject.prototype.isBlocking = function isBlocking() {
    return notWalkableStructure(this) && !accessibleRampart(this);
};
