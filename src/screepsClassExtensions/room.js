let {forArray, mapArray} = require('js-utils');
let {cache} = require('cache');

Room.prototype.cachedFind = function cachedFind(type) {
    let cacheKey = `${this.name}-findCache`;
    cache.data[cacheKey] = cache.data[cacheKey] || {};
    cache.data[cacheKey][type] = cache.data[cacheKey][type] || this.find(type);

    return cache.data[cacheKey][type];
};

Room.prototype.findMyStructures = function findMyStructures(type) {
    let cacheKey = `${this.name}-myStructureCache`;
    if (cache.data[cacheKey] === undefined) {
        cache.data[cacheKey] = {};
        forArray(this.cachedFind(FIND_MY_STRUCTURES), structure => {
            cache.data[cacheKey][structure.structureType] = cache.data[cacheKey][structure.structureType] || [];
            cache.data[cacheKey][structure.structureType].push(structure);
        });
    }

    return cache.data[cacheKey][type] || [];
};

Room.prototype.findStructures = function findStructures(type) {
    let cacheKey = `${this.name}-structureCache`;
    if (cache.data[cacheKey] === undefined) {
        cache.data[cacheKey] = {};
        forArray(this.cachedFind(FIND_STRUCTURES), structure => {
            cache.data[cacheKey][structure.structureType] = cache.data[cacheKey][structure.structureType] || [];
            cache.data[cacheKey][structure.structureType].push(structure);
        });
    }

    return cache.data[cacheKey][type] || [];
};

let containerCache = {};

Room.prototype.findContainers = function findContainers() {
    let cacheKey = `${this.name}-containers`;
    if (cache.data[cacheKey] === undefined) {
        let structures = this.cachedFind(FIND_STRUCTURES);
        if (!containerCache[this.name] || containerCache[this.name].checkSum !== structures.length) {
            containerCache[this.name] = {
                checkSum:     structures.length,
                containerIds: mapArray(this.findStructures(STRUCTURE_CONTAINER), structure => structure.id)
            };
        }

        cache.data[cacheKey] = mapArray(containerCache[this.name].containerIds, structureId => Game.getObjectById(structureId));
    }

    return cache.data[cacheKey];
};

let xyCache = {};

Room.getXY = function getXY(roomName) {
    if (xyCache[roomName] === undefined) {
        let [, hor, x, ver, y] = roomName.match(/^(\w)(\d+)(\w)(\d+)$/);

        x = hor === 'W' ? -x - 1 : +x;
        y = ver === 'N' ? -y - 1 : +y;

        xyCache[roomName] = [x, y];
    }

    return xyCache[roomName];
};

let roomDistanceCache = {};

Room.getDistance = function getDistance(firstRoomName, secondRoomName, continuous = false) {
    let cacheKey = `${firstRoomName}-${secondRoomName}-${continuous}`;
    if (roomDistanceCache[cacheKey] === undefined) {
        let [x1, y1] = Room.getXY(firstRoomName);
        let [x2, y2] = Room.getXY(secondRoomName);
        let dx = Math.abs(x2 - x1);
        let dy = Math.abs(y2 - y1);
        if (continuous === true) {
            let worldSize = Game.map.getWorldSize();
            dx = Math.min(worldSize - dx, dx);
            dy = Math.min(worldSize - dy, dy);
        }

        roomDistanceCache[cacheKey] = Math.max(dx, dy);
    }

    return roomDistanceCache[cacheKey];
};

Room.inRange = function inRange(firstRoomName, secondRoomName, range) {
    let [x1, y1] = Room.getXY(firstRoomName);
    let [x2, y2] = Room.getXY(secondRoomName);

    return Math.abs(x2 - x1) <= range && Math.abs(y2 - y1) <= range;
};
