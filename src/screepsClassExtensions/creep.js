let {cache} = require('cache');

Creep.prototype.toString = function toString() {
    return `[${(this.name ? this.name : this.id)} ${this.pos.toString(this.id)}]`;
};

Creep.prototype.getBodyPartCount = function getBodyPartCount(type) {
    let cacheKey = `${this.id}-bodyPartCount`;
    if (cache.data[cacheKey] === undefined) {
        cache.data[cacheKey] = {};
        let index = this.body.length;
        while (index-- > 0) {
            let bodyElement = this.body[index];
            if (bodyElement.hits <= 0) {
                break;
            } else {
                cache.data[cacheKey][bodyElement.type] = cache.data[cacheKey][bodyElement.type] || 0;
                cache.data[cacheKey][bodyElement.type]++;
            }
        }
    }

    return cache.data[cacheKey][type] || 0;
};
