let {sumObject} = require('js-utils');
let {cache} = require('cache');

Structure.prototype.toString = function toString() {
    return `[structure (${this.structureType}) #${this.id} ${this.pos.toString(this.id, `structures.${ this.id}`)}]`;
};

Structure.prototype.getStoreSum = function getStoreSum() {
    let cacheKey = `${this.id}-storeSum`;
    if (cache.data[cacheKey] === undefined) {
        cache.data[cacheKey] = sumObject(this.store);
    }

    return cache.data[cacheKey];
};
