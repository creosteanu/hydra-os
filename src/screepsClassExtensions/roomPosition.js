let {findArray} = require('js-utils');

RoomPosition.prototype.label = function label() {
    return `${this.x},${this.y},${this.roomName}`;
};

RoomPosition.fromLabel = function fromLabel(label) {
    return new RoomPosition(...label.split(','));
};

RoomPosition.prototype.nearTo = function nearTo(pos) {
    return this.roomName === pos.roomName && Math.abs(this.x - pos.x) <= 1 && Math.abs(this.y - pos.y) <= 1;
};

RoomPosition.prototype.inRange = function inRange(pos, range) {
    return this.roomName === pos.roomName && Math.abs(this.x - pos.x) <= range && Math.abs(this.y - pos.y) <= range;
};

RoomPosition.prototype.getRange = function getRange(pos) {
    return Math.max(Math.abs(this.x - pos.x), Math.abs(this.y - pos.y));
};

RoomPosition.prototype.equals = function equals(pos) {
    return this.roomName === pos.roomName && this.x === pos.x && this.y === pos.y;
};

RoomPosition.prototype.onExit = function onExit() {
    return this.x === 0 || this.x === 49 || this.y === 0 || this.y === 49;
};

RoomPosition.prototype.toString = function toString(id) {
    let onClick = id ? `angular.element('body').injector().get('RoomViewPendingSelector').set('${id}');` : '';

    return `<a href="#!/room/${this.roomName}" onClick="${onClick}">[${this.roomName} ${this.x},${this.y}]</a>`;
};

RoomPosition.prototype.isWalkable = function isWalkable() {
    if (this.lookFor(LOOK_TERRAIN)[0] === 'wall') {
        return false;
    }

    if (Game.rooms[this.roomName]) {
        if (findArray(this.lookFor(LOOK_STRUCTURES), structure => structure.isBlocking())) {
            return false;
        }

        if (findArray(this.lookFor(LOOK_CONSTRUCTION_SITES), construction => construction.isBlocking())) {
            return false;
        }
    }

    return true;
};

RoomPosition.prototype.getNearbyPositions = function getNearbyPositions(range = 1) {
    let positions = [];

    for (let x = -range; x <= range; x++) {
        for (let y = -range; y <= range; y++) {
            let position = new RoomPosition(this.x + x, this.y + y, this.roomName);

            if (position.x > 0 && position.x < 49 && position.y > 0 && position.y < 49) {
                positions.push(position);
            }
        }
    }

    return positions;
};

let xyToDirection = [[TOP_LEFT, TOP, TOP_RIGHT], [LEFT, 0, RIGHT], [BOTTOM_LEFT, BOTTOM, BOTTOM_RIGHT]];
RoomPosition.prototype.getDirection = function getDirection(pos) {
    if (this.roomName !== pos.roomName) {
        return 0;
    }

    let dy = pos.y - this.y + 1;
    let dx = pos.x - this.x + 1;

    return xyToDirection[dy][dx];
};
