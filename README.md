# HYDRA OS

## Commands

- install dependencies: yarn
- run tests and eslint: yarn test
- build: grunt build

## Process architecture

- named **operation** to avoid naming conflict with reserved word
- each operation is an entry in Memory.operations
- each operation will execute the code linked to Memory.operations.id.name or Memory.operations.id
- operation execution is encapsulated by a helper to limit propagation of errors
- communication between operations is done through dedicated api files located in local node_modules folders

## Example operations

4 example operations are included:
- spawning
- pioneering - multi purpose worker
- farming - high level process to decide which sources to farm
- farmer - low level process handling the effective farming

## Automated testing

Hydra was built around the concept of facilitating automated testing. The core Hydra AI is developed with 100% statement coverage and >90% branch coverage.

## Auto versioning

Hydra was built to integrate with gitlab ci. If you fork the project and setup a CI_TOKEN variable in your repo, it will automatically manage patch versions. Alternatively use the inbuilt package json methods.

## To Do before official launch

- minimize custom js-utils functions and introduce lodash dependency to lower barrier of entry
- extract the automated test mock classes into their own repo
- expand documentation
