require('./screepsMocks');

beforeEach(() => {
    jest.resetModules();
    require('./screepsMocks');
    require('../src/screepsClassExtensions');
    require('./data');
});

afterEach(() => {
    jest.restoreAllMocks();
});
