new Room({name: 'W15S34', energyCapacityAvailable: 2000, energyAvailable: 1000});
new StructureController({id: 'controllerId', level: 1, pos: new RoomPosition(24, 24, 'W15S34')});
new StructureSpawn({id: 'Spawn1', name: 'W15S34-0', energy: SPAWN_ENERGY_START, pos: new RoomPosition(10, 10, 'W15S34')});
