/**
 * An energy source object.
 * Can be harvested by creeps with a WORK body part.
 *
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/203079211-Source}
 */
class Source extends RoomObject {
    constructor(init = {}) {
        super(init);

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079211-Source#id}
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * The remaining amount of energy.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079211-Source#energy}
         *
         * @type {number}
         */
        this.energy = init.energy || 0;

        /**
         * The total amount of energy in the source.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079211-Source#energyCapacity}
         *
         * @type {number}
         */
        this.energyCapacity = init.energyCapacity || SOURCE_ENERGY_NEUTRAL_CAPACITY;

        /**
         * The remaining time after which the source will be refilled.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079211-Source#ticksToRegeneration}
         *
         * @type {number}
         */
        this.ticksToRegeneration = this.ticksToRegeneration || 0;

        if (this.room) {
            this.room._sources.push(this);
        }
    }
}

global.Source = Source;
