class Order {
    constructor(init) {
        /**
         * The unique order ID.
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * The order creation time in game ticks.
         *
         * @type {number}
         */
        this.created = 0;

        /**
         * Whether this order is active and visible to other players.
         * An order can become non-active when the terminal doesn't have enough resources to sell or you are out of credits to buy.
         * Whether this order is active and visible to other players.
         * An order can become non-active when the terminal doesn't have enough resources to sell or you are out of credits to buy.
         *
         * @type {boolean}
         */
        this.active = true;

        /**
         * Either ORDER_SELL or ORDER_BUY
         *
         * @type {ORDER_SELL|ORDER_BUY}
         */
        this.type = 'sell';

        /**
         * Either one of the RESOURCE_* constants or SUBSCRIPTION_TOKEN
         *
         * @type {string|SUBSCRIPTION_TOKEN}
         */
        this.resourceType = '';

        /**
         * The room where this order is placed.
         * @type {string}
         */
        this.roomName = '';

        /**
         * Currently available amount to trade.
         * @type {number}
         */
        this.amount = 0;

        /**
         * How many resources are left to trade via this order.
         * @type {number}
         */
        this.remainingAmount = 0;

        /**
         * Initial order amount.
         * @type {number}
         */
        this.totalAmount = 0;

        /**
         * Price
         * @type {number}
         */
        this.price = 0;
    }
}

global.Order = Order;
