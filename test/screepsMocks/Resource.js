/**
 * A dropped piece of resource.
 * It will decay after a while if not picked up.
 * Dropped resource pile decays for ceil(amount/1000) units per tick.
 *
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/203016362-Resource}
 */
class Resource extends RoomObject {
    constructor(init) {
        super(init);

        /**
         * The amount of resource units containing.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016362-Resource#amount}
         *
         * @type {number}
         */
        this.amount = init.amount || 0;

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016362-Resource#id}
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * One of the RESOURCE_* constants.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016362-Resource#resourceType}
         *
         * @type {string}
         */
        this.resourceType = init.resourceType || '';

        if (this.room) {
            this.room._resources.push(this);
        }
    }
}

global.Resource = Resource;
