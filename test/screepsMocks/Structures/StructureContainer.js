/**
 * A small container that can be used to store resources.
 * This is a walkable structure.
 * All dropped resources automatically goes to the container at the same tile.
 *
 * @class
 * @extends {Structure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/208435885-StructureContainer}
 */
class StructureContainer extends Structure {
    constructor(init = {}) {
        super(init);

        this.structureType = STRUCTURE_CONTAINER;
        this.hits = init.hits || CONTAINER_HITS;
        this.hitsMax = CONTAINER_HITS;

        /**
         * An object with the structure contents.
         * Each object key is one of the RESOURCE_* constants, values are resources amounts.
         * Use _.sum(structure.store) to get the total amount of contents.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208435885-StructureContainer#store}
         *
         * @type {Array<string, number>}
         */
        this.store = init.store || {};

        /**
         * The total amount of resources the structure can contain.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208435885-StructureContainer#storeCapacity}
         *
         * @type {number}
         */
        this.storeCapacity = CONTAINER_CAPACITY;
    }
}

global.StructureContainer = StructureContainer;
