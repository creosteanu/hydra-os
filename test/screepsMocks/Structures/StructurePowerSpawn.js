/* eslint-disable no-unused-vars */

/**
 * Processes power into your account, and spawns power creeps with special unique powers (in development).
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn}
 */
class StructurePowerSpawn extends OwnedStructure {
    constructor(init) {
        super(init);

        this.structureType = STRUCTURE_POWER_SPAWN;

        /**
         * The amount of energy containing in this structure.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#energy}
         *
         * @type {number}
         */
        this.energy = 0;

        /**
         * The total amount of energy this structure can contain.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#energyCapacity}
         *
         * @type {number}
         */
        this.energyCapacity = 0;

        /**
         * The amount of power containing in this structure.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#power}
         *
         * @type {number}
         */
        this.power = 0;

        /**
         * The total amount of power this structure can contain.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#powerCapacity}
         *
         * @type {number}
         */
        this.powerCapacity = 0;
    }

    /**
     * Create a power creep.
     * @note This method is under development.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#createPowerCreep}
     *
     * @type {function}
     *
     * @param {string} roomName The name of the power creep.
     *
     * @return {void}
     */
    createPowerCreep(roomName) {
    }

    /**
     * Register power resource units into your account.
     * Registered power allows to develop power creeps skills.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/208436585-StructurePowerSpawn#processPower}
     *
     * @type {function}
     *
     * @return {number|OK|ERR_NOT_ENOUGH_RESOURCES|ERR_RCL_NOT_ENOUGH}
     */
    processPower() {
    }
}

global.StructurePowerSpawn = StructurePowerSpawn;
