/**
 * Claim this structure to take control over the room.
 * The controller structure cannot be damaged or destroyed.
 * It can be addressed by Room.controller property.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController}
 */
class StructureController extends OwnedStructure {
    constructor(init) { // eslint-disable-line max-statements
        super(init);

        this.structureType = STRUCTURE_CONTROLLER;

        /**
         * Ticks left before another safeMode can be used
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#safeModeCooldown}
         *
         * @type {number}
         */
        this.safeModeCooldown = init.safeModeCooldown || 0;

        /**
         * The number of available safeMode activations
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#safeModeAvailable}
         *
         * @type {number}
         */
        this.safeModeAvailable = init.safeModeAvailable || 0;

        /**
         * Returns if safeMode is active. If not this will return undefined, not false.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#safeMode}
         *
         * @type {Boolean|undefined}
         */
        this.safeMode = init.safeMode;

        /**
         * Current controller level, from 0 to 8.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#level}
         *
         * @type {number}
         */
        this.level = init.level || 0;

        /**
         * The current progress of upgrading the controller to the next level.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#progress}
         *
         * @type {number}
         */
        this.progress = init.progress || 0;

        /**
         * The progress needed to reach the next level.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#progressTotal}
         *
         * @type {number}
         */
        this.progressTotal = init.progressTotal || 0;

        /**
         * An object with the controller reservation info if present
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#reservation}
         *
         * @type {null|{username: string, ticksToEnd: number}}
         */
        this.reservation = init.reservation || {};

        /**
         * The amount of game ticks when this controller will lose one level.
         * This timer can be reset by using Creep.upgradeController.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#ticksToDowngrade}
         *
         * @type {number}
         */
        this.ticksToDowngrade = this.ticksToDowngrade || 0;

        /**
         * The amount of game ticks while this controller cannot be upgraded due to attack.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#upgradeBlocked}
         *
         * @type {number}
         */
        this.upgradeBlocked = this.upgradeBlocked || 0;

        if (this.room) {
            this.room.controller = this;
        }
    }

    /**
     * Triggers the activation of a saveMode if possible and available
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#activateSafeMode}
     *
     * @type {function}
     *
     * @return {OK|ERR_NOT_OWNER|ERR_NOT_ENOUGH_RESOURCES|ERR_TIRED}
     */
    activateSafeMode() {
        return !this.safeModeCooldown && !this.upgradeBlocked && this.safeModeAvailable ? OK : ERR_TIRED;
    }

    /**
     * Make your claimed controller neutral again.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/207711889-StructureController#unclaim}
     *
     * @type {function}
     *
     * @return {number|OK|ERR_NOT_OWNER}
     */
    unclaim() {
    }
}

global.StructureController = StructureController;
