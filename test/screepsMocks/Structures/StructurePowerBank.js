/**
 * Non-player structure.
 * Contains power resource which can be obtained by destroying the structure.
 * Hits the attacker creep back on each attack.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207712729-StructurePowerBank}
 */
class StructurePowerBank extends OwnedStructure {
    constructor(init) {
        super(init);

        this.structureType = STRUCTURE_POWER_BANK;

        /**
         * The amount of power containing.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207712729-StructurePowerBank#power}
         *
         * @type {number}
         */
        this.power = init.power || 0;

        /**
         * The amount of game ticks when this structure will disappear.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207712729-StructurePowerBank#ticksToDecay}
         *
         * @type {number}
         */
        this.ticksToDecay = 0;
    }
}

global.StructurePowerBank = StructurePowerBank;
