/**
 * Contains energy which can be spent on spawning bigger creeps.
 * Extensions can be placed anywhere in the room, any spawns will be able to use them regardless of distance.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207711949-StructureExtension}
 */
class StructureExtension extends OwnedStructure {
    constructor(init = {}) {
        super(init);

        this.structureType = STRUCTURE_EXTENSION;
        this.hits = init.hits || EXTENSION_HITS;
        this.hitsMax = EXTENSION_HITS;

        /**
         * The amount of energy containing in the extension.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711949-StructureExtension#energy}
         *
         * @type {number}
         */
        this.energy = init.energy || 0;

        /**
         * The total amount of energy the extension can contain.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207711949-StructureExtension#energyCapacity}
         *
         * @type {number}
         */
        this.energyCapacity = init.energyCapacity || 50;
    }
}

global.StructureExtension = StructureExtension;
