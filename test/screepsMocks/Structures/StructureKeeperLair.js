/**
 * Non-player structure.
 * Spawns NPC Source Keepers that guards energy sources and minerals in some rooms.
 * This structure cannot be destroyed.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207712119-StructureKeeperLair}
 */
class StructureKeeperLair extends OwnedStructure {
    constructor(init) {
        init.my = false;

        super(init);

        this.structureType = STRUCTURE_KEEPER_LAIR;

        /**
         * Time to spawning of the next Source Keeper.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207712119-StructureKeeperLair#ticksToSpawn}
         *
         * @type {number}
         */
        this.ticksToSpawn = init.ticksToSpawn || 0;
    }
}

global.StructureKeeperLair = StructureKeeperLair;
