/**
 * A structure that can store huge amount of resource units.
 * Only one structure per room is allowed that can be addressed by Room.storage property.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/208436805-StructureStorage}
 */
class StructureStorage extends OwnedStructure {
    constructor(init = {}) {
        super(init);

        this.structureType = STRUCTURE_STORAGE;
        this.hits = init.hits || STORAGE_HITS;
        this.hitsMax = STORAGE_HITS;

        /**
         * An object with the storage contents.
         * Each object key is one of the RESOURCE_* constants, values are resources amounts.
         * Use _.sum(structure.store) to get the total amount of contents.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436805-StructureStorage#store}
         *
         * @type {Array<string, number>}
         */
        this.store = init.store || {};

        /**
         * The total amount of resources the storage can contain.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208436805-StructureStorage#storeCapacity}
         *
         * @type {number}
         */
        this.storeCapacity = STORAGE_CAPACITY;

        if (this.room) {
            this.room.storage = this;
        }
    }
}

global.StructureStorage = StructureStorage;
