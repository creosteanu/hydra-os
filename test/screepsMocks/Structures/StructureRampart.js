/**
 * Blocks movement of hostile creeps, and defends your creeps and structures on the same tile.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207712959-StructureRampart}
 */
class StructureRampart extends OwnedStructure {
    constructor(init = {}) {
        super(init);

        this.structureType = STRUCTURE_RAMPART;

        /**
         * The amount of game ticks when this rampart will lose some hit points.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207712959-StructureRampart#ticksToDecay}
         *
         * @type {number}
         */
        this.ticksToDecay = 0;

        this.isPublic = init.isPublic || false;
    }
}

global.StructureRampart = StructureRampart;
