/**
 * Decreases movement cost to 1.
 * Using roads allows creating creeps with less MOVE body parts.
 *
 * @class
 * @extends {Structure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207713089-StructureRoad}
 */
class StructureRoad extends Structure {
    constructor(init) {
        super(init);

        this.structureType = STRUCTURE_ROAD;
        this.hitsMax = ROAD_HITS;

        /**
         * The amount of game ticks when this road will lose some hit points.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207713089-StructureRoad#ticksToDecay}
         *
         * @type {number}
         */
        this.ticksToDecay = 0;
    }
}

global.StructureRoad = StructureRoad;
