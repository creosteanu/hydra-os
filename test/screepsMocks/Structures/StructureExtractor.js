/**
 * Allows to harvest a mineral deposit.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207715739-StructureExtractor}
 */
class StructureExtractor extends OwnedStructure {
    constructor(init) {
        super(init);

        this.structureType = STRUCTURE_EXTRACTOR;
    }
}

global.StructureExtractor = StructureExtractor;
