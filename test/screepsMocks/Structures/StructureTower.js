/* eslint-disable no-unused-vars */

/**
 * Remotely attacks or heals creeps, or repairs structures.
 * Can be targeted to any object in the room.
 * However, its effectiveness highly depends on the distance.
 * Each action consumes energy.
 *
 * @class
 * @extends {OwnedStructure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower}
 */
class StructureTower extends OwnedStructure {
    constructor(init) {
        super(init);

        this.structureType = STRUCTURE_TOWER;

        /**
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower#energy}
         *
         * @type {number}
         */
        this.energy = init.energy !== undefined ? init.energy : TOWER_CAPACITY;

        /**
         * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower#energyCapacity}
         *
         * @type {number}
         */
        this.energyCapacity = TOWER_CAPACITY;
    }

    /**
     * Remotely attack any creep in the room.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower#attack}
     *
     * @type {function}
     *
     * @param {Creep} target The target creep.
     *
     * @return {number|OK|ERR_NOT_ENOUGH_RESOURCES|ERR_INVALID_TARGET|ERR_RCL_NOT_ENOUGH}
     */
    attack(target) {
    }

    /**
     * Remotely heal any creep in the room.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower#heal}
     *
     * @type {function}
     *
     * @param {Creep} target The target creep.
     *
     * @return {number|OK|ERR_NOT_ENOUGH_RESOURCES|ERR_INVALID_TARGET|ERR_RCL_NOT_ENOUGH}
     */
    heal(target) {
    }

    /**
     * Remotely repair any structure in the room.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/208437105-StructureTower#repair}
     *
     * @type {function}
     *
     * @param {Spawn|Structure} target The target structure.
     *
     * @return {number|OK|ERR_NOT_ENOUGH_RESOURCES|ERR_INVALID_TARGET|ERR_RCL_NOT_ENOUGH}
     */
    repair(target) {
    }
}

global.StructureTower = StructureTower;
