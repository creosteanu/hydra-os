/**
 * The base prototype object of all structures.
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://docs.screeps.com/api/#Tombstone.room}
 */
class Tombstone extends RoomObject {
    constructor(init) {
        super(init);

        /**
         * The amount of game ticks before this tombstone decays.
         *
         * @type {number}
         */
        this.ticksToDecay = init.ticksToDecay || 0;

        /**
         * Time of death.
         *
         * @type {number}
         */
        this.deathTime = init.deathTime || Game.time;

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * An object with the structure contents.
         * Each object key is one of the RESOURCE_* constants, values are resources amounts.
         * Use _.sum(structure.store) to get the total amount of contents.
         *
         * @type {Array<string, number>}
         */
        this.store = init.store || {};

        if (this.room) {
            this.room._tombstones.push(this);
        }
    }
}

global.Tombstone = Tombstone;
