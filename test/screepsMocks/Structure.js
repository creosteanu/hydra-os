/* eslint-disable no-unused-vars */

/**
 * The base prototype object of all structures.
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure}
 */
class Structure extends RoomObject {
    constructor(init = {}) {
        super(init);

        /**
         * The current amount of hit points of the structure.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#hits}
         *
         * @type {number}
         */
        this.hits = init.hits || 0;

        /**
         * The total amount of hit points of the structure.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#hitsMax}
         *
         * @type {number}
         */
        this.hitsMax = init.hitsMax || 0;

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#id}
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * One of the STRUCTURE_* constants.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#structureType}
         *
         * @type {string}
         */
        this.structureType = init.structureType || '';

        if (this.room) {
            this.room._structures.push(this);
        }
    }

    /**
     * Destroy this structure immediately.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#destroy}
     *
     * @type {function}
     *
     * @return {number|OK|ERR_NOT_OWNER}
     */
    destroy() {
    }

    /**
     * Check whether this structure can be used.
     * If the room controller level is not enough,
     * then this method will return false, and the structure will be highlighted with red in the game.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#isActive}
     *
     * @type {function}
     *
     * @return {boolean}
     */
    isActive() {
    }

    /**
     * Toggle auto notification when the structure is under attack.
     * The notification will be sent to your account email.
     * Turned on by default.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/203079221-Structure#notifyWhenAttacked}
     *
     * @type {function}
     *
     * @param {boolean} enabled Whether to enable notification or disable.
     *
     * @return {number|OK|ERR_NOT_OWNER|ERR_INVALID_ARGS}
     */
    notifyWhenAttacked(enabled) {
    }

    _removeMock() {
        this.room._structures.splice(this.room._structures.indexOf(this), 1);
        delete Game._objects[this.id];
    }
}

global.Structure = Structure;
