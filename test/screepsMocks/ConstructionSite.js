/**
 * A site of a structure which is currently under construction.
 * A construction site can be created using the 'Construct' button at the left of the game field or the Room.createConstructionSite method.
 * To build a structure on the construction site, give a worker creep some amount of energy and perform Creep.build action.
 *
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite}
 */
class ConstructionSite extends RoomObject {
    constructor(init = {}) {
        super(init);

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#id}
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * Whether this is your own construction site.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#my}
         *
         * @type {boolean}
         */
        this.my = init.my !== undefined ? init.my : true;

        /**
         * An object with the structure’s owner info
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#owner}
         *
         * @type {{username: ""}}
         */
        this.owner = init.owner || {username: 'Atavus'};

        /**
         * The current construction progress.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#progress}
         *
         * @type {number}
         */
        this.progress = init.progress || 0;

        /**
         * The total construction progress needed for the structure to be built.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#progressTotal}
         *
         * @type {number}
         */
        this.progressTotal = init.progressTotal || 0;

        /**
         * One of the STRUCTURE_* constants.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#structureType}
         *
         * @type {string}
         */
        this.structureType = init.structureType || '';

        if (this.room && this.my) {
            this.room._myConstructionSites.push(this);
            Game.constructionSites[this.id] = this;
        }
    }

    /**
     * Remove the construction site.
     *
     * @see {@link http://support.screeps.com/hc/en-us/articles/203016342-ConstructionSite#remove}
     *
     * @type {function}
     *
     * @return {number|OK|ERR_NOT_OWNER}
     */
    remove() {
    }
}

global.ConstructionSite = ConstructionSite;
