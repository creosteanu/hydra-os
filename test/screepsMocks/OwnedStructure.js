/**
 * The base prototype for a structure that has an owner.
 * Such structures can be found using FIND_MY_STRUCTURES and FIND_HOSTILE_STRUCTURES constants.
 *
 * @class
 * @extends {Structure}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/207710979-OwnedStructure}
 */
class OwnedStructure extends Structure {
    constructor(init = {}) {
        super(init);

        /**
         * Whether this is your own structure.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207710979-OwnedStructure#my}
         *
         * @type {boolean}
         */
        this.my = init.my !== undefined ? init.my : true;

        /**
         * An object with the structure’s owner info
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/207710979-OwnedStructure#owner}
         *
         * @type {{username: string}}
         */
        this.owner = init.owner !== undefined ? init.owner : {username: 'Atavus'};

        if (this.room) {
            if (this.my) {
                this.room._myStructures.push(this);
            } else {
                this.room._hostileStructures.push(this);
            }
        }

        if (this.my) {
            Game.structures[this.id] = this;
        }
    }

    _removeMock() {
        super._removeMock();

        this.room._myStructures.splice(this.room._myStructures.indexOf(this), 1);
        delete Game.structures[this.id];
    }
}

global.OwnedStructure = OwnedStructure;
