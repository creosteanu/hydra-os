/**
 * A nuke landing position.
 * This object cannot be removed or modified.
 * You can find incoming nukes in the room using the FIND_NUKES constant.
 * @class
 * @extends {RoomObject}
 *
 * @see {@link http://support.screeps.com/hc/en-us/articles/208488525-Nuke}
 */
class Nuke extends RoomObject {
    constructor(init) {
        super(init);

        /**
         * A unique object identificator.
         * You can use Game.getObjectById method to retrieve an object instance by its id.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208488525-Nuke#id}
         *
         * @type {string}
         */
        this.id = init.id;
        Game._generateObjectId(this);

        /**
         * The name of the room where this nuke has been launched from.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208488525-Nuke#launchRoomName}
         *
         * @type {string}
         */
        this.launchRoomName = '';

        /**
         * The remaining landing time.
         *
         * @see {@link http://support.screeps.com/hc/en-us/articles/208488525-Nuke#timeToLand}
         *
         * @type {number}
         */
        this.timeToLand = 0;
    }
}

global.Nuke = Nuke;
